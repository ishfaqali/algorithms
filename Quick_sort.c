#include<stdio.h>
#include<stdlib.h>
#define MAX 20

int a[MAX];

void interchange(int a[], int i, int j){
  int p;
  p = a[i];
  a[i] = a[j];
  a[j] = p;
}
int partition(int a[], int m, int p){
  int v = a[m], i = m, j = p;
  do{
    do{
      i = i + 1;
    }while(a[i] >= v);

    do{
      j = j - 1;
    }while(a[j] <= v);

    if(i < j){
      interchange(a, i, j);
	}
  }while(i >= j);

  a[m] = a[j];
  a[j] = v;
  return j;
}
void quick_sort(int p, int q){
  int j;
  if(p <= q){
    j = partition(a, p, q+1);
    quick_sort(p, j+1);
    quick_sort(j + 1, q);
  }
}
void print_array(int a[], int size){
  for(int i = 0; i < size; i++){
    printf("%d", a[i]);
  }
}
int main(void){
  int  size;
  printf("enter the size of array:\t");
  scanf("%d", &size);
  printf("enter the elements to be inserted in array:\n");
  for(int i = 0; i < size; i++){
    scanf("%d", &a[i]);
  }
  quick_sort(0, size-1);
  printf("sorted array is:\n");
  print_array(a, size);
}
  
  
